package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) 
	 { 
	 super(t); 
	 } 
	 public String toString() 
	 { 
	 // TODO: Complete this method 
	 return this.getValue() + ""; 
	 }
	@Override
	public Temperature toCelsius() {
		Temperature fah = new Celsius((float)((this.getValue()-32)*(5.0/9.0)));
		return fah;
	}
	@Override
	public Temperature toFahrenheit() {
		Temperature fah = new Fahrenheit(this.getValue());
		return fah;
	}
}
