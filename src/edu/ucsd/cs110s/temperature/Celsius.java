package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {
	public Celsius(float t) 
	 { 
	 super(t);  } 
	 public String toString() 
	 { 
	 // TODO: Complete this method 
	 return this.getValue() + ""; 
	 }
	@Override
	public Temperature toCelsius() {
		Temperature cel = new Celsius(this.getValue());
		return cel;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Temperature cel = new Fahrenheit((float)(this.getValue() * (9.0 / 5.0) + 32));
		return cel;
	} 
}
